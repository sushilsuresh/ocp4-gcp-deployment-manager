---
- name: OpenShift Install Playbook
  hosts: localhost
  tasks:

    # - name: Run OpenShift Install if and only if BOOTSTRAP_CLUSTER = true
    #   block:

    - name: Figure out openshift version
      command: oc version -o json
      register: reg_oc_version
      changed_when: false
      failed_when:
        - reg_oc_version.rc == 1
        - not reg_oc_version.stderr is search('Unable to connect to the serve')

    - name: Set ocp_version fact
      set_fact:
        ocp_version: "{{ oc_version }}"
      vars:
        oc_version: "{{ reg_oc_version.stdout | from_json | json_query('releaseClientVersion') }}"
      failed_when:

      # probably a better way fetch given the fact it is a json file.
    - name: Gather the infra_id from
      command: jq -r .infraID {{ install_dir }}/metadata.json
      register: reg_infra_id
      changed_when: false

    - name: Set fact infra_id
      set_fact:
        infra_id: "{{ reg_infra_id.stdout }}"

    - name: Get current gcloud deployments
      command: gcloud deployment-manager deployments  list --format=json
      register: reg_gcloud_deps
      changed_when: false

    - set_fact:
        gcloud_deployments: "{{ deployments }}"
      vars:
        deployments: "{{ reg_gcloud_deps.stdout | from_json | json_query('[*].name') }}"

    - name: Generate DM template for VPC
      include_role:
        name: gcp-dm-templates
        tasks_from: "vpc"
      vars:
        deployment: "{{ infra_id }}-vpc"
      when:
        - deployment not in gcloud_deployments

    - name: Create VPC deployment using DM template
      command: >-
        gcloud deployment-manager deployments
        create {{ deployment }}
        --config {{ deployment_template }}
      vars:
        deployment: "{{ infra_id }}-vpc"
        deployment_template: "{{ dm_templates_dir }}/01_vpc.yaml"
      when:
        - deployment not in gcloud_deployments

    - name: Generate DM template for infra
      include_role:
        name: gcp-dm-templates
        tasks_from: "infra"
      vars:
        deployment: "{{ infra_id }}-infra"
      when:
        - deployment not in gcloud_deployments

    - name: Create infra deployment using DM template
      command: >-
        gcloud deployment-manager deployments
        create {{ deployment }}
        --config {{ deployment_template }}
      vars:
        deployment: "{{ infra_id }}-infra"
        deployment_template: "{{ dm_templates_dir }}/02_infra.yaml"
      when:
        - deployment not in gcloud_deployments

    # NOTE: The below linked documenation page says this.
    #       https://docs.openshift.com/container-platform/4.4/installing/installing_gcp/installing-gcp-user-infra.html
    #       The templates do not create DNS entries due to limitations of
    #       Deployment Manager, so you must create them manually
    # TIP: Need to validate this or see if new deployment manager can handle
    #      creating dns records
    - name: Include role to create dns entires
      include_role:
        name: gcloud-dns
        tasks_from: "infra"
      vars:
        deployment: "{{ infra_id }}-infra"
      when:
        - deployment not in gcloud_deployments

    - name: Generate DM template for security
      include_role:
        name: gcp-dm-templates
        tasks_from: "security"
      vars:
        deployment: "{{ infra_id }}-security"
      when:
        - deployment not in gcloud_deployments

    - name: Create security deployment using DM template
      command: >-
        gcloud deployment-manager deployments
        create {{ deployment }}
        --config {{ deployment_template }}
      register: reg_security_deployment
      vars:
        deployment: "{{ infra_id }}-security"
        deployment_template: "{{ dm_templates_dir }}/03_security.yaml"
      when:
        - deployment not in gcloud_deployments

    - name: Create iam policy bindings for master/worker service accounts
      command: '{{ item }}'
      when:
        - reg_security_deployment.changed
      loop:
        - 'gcloud projects add-iam-policy-binding {{ project_id }} --member "serviceAccount:{{ master_sa }}" --role "roles/compute.instanceAdmin"'
        - 'gcloud projects add-iam-policy-binding {{ project_id }} --member "serviceAccount:{{ master_sa }}" --role "roles/compute.networkAdmin"'
        - 'gcloud projects add-iam-policy-binding {{ project_id }} --member "serviceAccount:{{ master_sa }}" --role "roles/compute.securityAdmin"'
        - 'gcloud projects add-iam-policy-binding {{ project_id }} --member "serviceAccount:{{ master_sa }}" --role "roles/iam.serviceAccountUser"'
        - 'gcloud projects add-iam-policy-binding {{ project_id }} --member "serviceAccount:{{ master_sa }}" --role "roles/storage.admin"'
        - 'gcloud projects add-iam-policy-binding {{ project_id }} --member "serviceAccount:{{ worker_sa }}" --role "roles/compute.viewer"'
        - 'gcloud projects add-iam-policy-binding {{ project_id }} --member "serviceAccount:{{ worker_sa }}" --role "roles/storage.admin"'

    - name: Include role to create rhcos image
      include_role:
        name: rhcos-image

    - name: Generate DM template for boostrap node
      include_role:
        name: gcp-dm-templates
        tasks_from: "bootstrap"
      vars:
        deployment: "{{ infra_id }}-bootstrap"
      when:
        - deployment not in gcloud_deployments

    - name: Create bootstrap deployment using DM template
      command: >-
        gcloud deployment-manager deployments
        create {{ deployment }}
        --config {{ deployment_template }}
      vars:
        deployment: "{{ infra_id }}-bootstrap"
        deployment_template: "{{ dm_templates_dir }}/04_bootstrap.yaml"
      when:
        - deployment not in gcloud_deployments

    # NOTE: The below linked documenation page says this.
    #       https://docs.openshift.com/container-platform/4.4/installing/installing_gcp/installing-gcp-user-infra.html
    #       The templates do not manage load balancer membership due to
    #       limitations of Deployment Manager, so you must add the bootstrap
    #       machine manually:
    # TIP: Need to validate this or see if new deployment manager can handle it
    - name: Include role for adding bootstrap node to compute target pool
      include_role:
        name: compute-target-pool
        tasks_from: "bootstrap"
      vars:
        deployment: "{{ infra_id }}-bootstrap"
      when:
        - deployment not in gcloud_deployments

    - name: Generate DM template for control plane nodes
      include_role:
        name: gcp-dm-templates
        tasks_from: "control_plane"
      vars:
        deployment: "{{ infra_id }}-control-plane"
      when:
        - deployment not in gcloud_deployments

    - name: Create control-plane deployment using DM template
      command: >-
        gcloud deployment-manager deployments
        create {{ deployment }}
        --config {{ deployment_template }}
      vars:
        deployment: "{{ infra_id }}-control-plane"
        deployment_template: "{{ dm_templates_dir }}/05_control_plane.yaml"
      when:
        - deployment not in gcloud_deployments

    # NOTE: The below linked documenation page says this.
    #       https://docs.openshift.com/container-platform/4.4/installing/installing_gcp/installing-gcp-user-infra.html
    #       The templates do not manage load balancer membership due to
    #       limitations of Deployment Manager, so you must add the bootstrap
    #       machine manually:
    # TIP: Need to validate this or see if new deployment manager can handle it
    - name: Include role for adding bootstrap node to compute target pool
      include_role:
        name: compute-target-pool
        tasks_from: "control_plane"
      vars:
        deployment: "{{ infra_id }}-control-plane"
      when:
        - deployment not in gcloud_deployments

    # NOTE: The below linked documenation page says this.
    #       https://docs.openshift.com/container-platform/4.4/installing/installing_gcp/installing-gcp-user-infra.html
    #       The templates do not create DNS entries due to limitations of
    #       Deployment Manager, so you must create them manually
    # TIP: Need to validate this or see if new deployment manager can handle
    #      creating dns records
    - name: Include role to create dns entires
      include_role:
        name: gcloud-dns
        tasks_from: "control_plane"
      vars:
        deployment: "{{ infra_id }}-control-plane"
      when:
        - deployment not in gcloud_deployments

    - name: Generate DM template for worker nodes
      include_role:
        name: gcp-dm-templates
        tasks_from: "worker"
      vars:
        deployment: "{{ infra_id }}-worker"
      when:
        - deployment not in gcloud_deployments

    - name: Create worker deployment using DM template
      command: >-
        gcloud deployment-manager deployments
        create {{ deployment }}
        --config {{ deployment_template }}
      vars:
        deployment: "{{ infra_id }}-worker"
        deployment_template: "{{ dm_templates_dir }}/06_worker_patched.yaml"
      when:
        - deployment not in gcloud_deployments

    # - name: Set Gitlab variable BOOTSTRAP_CLUSTER to false
    #   uri:
    #     url: "{{ gitlab_url }}"
    #     method: PUT
    #     body_format: json
    #     body:
    #       key: "BOOTSTRAP_CLUSTER"
    #       value: "false"
    #       variable_type: "env_var"
    #       protected: false
    #       masked: false
    #       environment_scope: "*"
    #     headers:
    #       PRIVATE-TOKEN: "{{ gitlab_api_token }}"
    #     status_code:
    #       - 200
    #       - 201
    #   vars:
    #     gitlab_url: "{{ gitlab_api_url }}/projects/{{ gitlab_project_id }}/variables/BOOTSTRAP_CLUSTER"

      # when:
      #   - bootstrap_cluster
