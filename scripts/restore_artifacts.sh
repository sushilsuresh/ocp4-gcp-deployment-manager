#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset

echo "restoring artifacts"

gsutil -m cp -r ${ARTIFACTS_BUCKET_URL}* tmp/
