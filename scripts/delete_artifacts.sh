#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset

echo "restoring artifacts"

if [ "${DESTROY_CLUSTER}" == "true" ]
then
    gsutil -m rm -r ${ARTIFACTS_BUCKET_URL}**
fi
