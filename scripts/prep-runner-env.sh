#!/usr/bin/env bash

set -o errexit
set -o pipefail
# set -o nounset

SSH_KEY_FILE="${HOME}/.ssh/core_rsa"
GCLOUD_SA_JSON="${HOME}/.gcloud/sa-key.json"
KUBECONFIG_FILE="${HOME}/.kube/config"

mkdir -p ${HOME}/.gcloud
echo ${GCP_SA_KEY_B64} | base64 -d > ${GCLOUD_SA_JSON}
chmod 600 ${GCLOUD_SA_JSON}

mkdir -p ${HOME}/.ssh
echo ${SSK_KEY_B64} | base64 -d > ${SSH_KEY_FILE}
chmod 600 ${SSH_KEY_FILE}

mkdir -p ${HOME}/.kube
if [ ! -z ${KUBECONFIG_B64} ]
then
  echo ${KUBECONFIG_B64} | base64 -d > ${KUBECONFIG_FILE}
  chmod 600 ${KUBECONFIG_FILE}
fi
