#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset

# Gather list of pnending csr names
PENDING_CSR=$(oc get csr -o json | jq -r '.items[] | if .status == {} then .metadata.name else empty end')

if [ ! -z "${PENDING_CSR}" ]
then
    for cert in ${PENDING_CSR}
    do
        oc adm certificate approve ${cert} > /dev/null 2>&1
    done
fi

# Get the list of nodes in Ready Status
oc get nodes | grep -w -c 'Ready'
