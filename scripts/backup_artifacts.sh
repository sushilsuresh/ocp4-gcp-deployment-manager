#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset

echo "backing up artifacts"

gsutil -m cp -r tmp/* ${ARTIFACTS_BUCKET_URL}
